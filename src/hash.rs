use std::convert::TryFrom;
use std::fmt;

use crate::{GeoHashEncodeError, GeoHashDecodeError};
use crate::{
    GEO_LONG_MAX, GEO_LONG_MIN, GEO_LAT_MAX, GEO_LAT_MIN, GEO_STEP_MAX, Point,
};

pub enum GeoDirection {
    North,
    East,
    West,
    South,
    SouthWest,
    SouthEast,
    NortWest,
    NortEast,
}

#[derive(Default, Copy, Clone, Debug, PartialEq)]
pub struct GeoHashBits {
    bits: u64,
    step: u8,
}

impl GeoHashBits {
    pub fn from_step_and_bits(step: u8, bits: u64) -> GeoHashBits {
        GeoHashBits {
            step: step,
            bits: bits,
        }
    }

    pub fn is_zero(&self) -> bool {
        self.bits == 0 && self.step == 0
    }

    pub fn neighbors(self) -> GeoHashNeighbors {
        GeoHashNeighbors {
            east: geohash_move_xy(self, 1, 0),
            west: geohash_move_xy(self, -1, 0),
            south: geohash_move_xy(self, 0, -1),
            north: geohash_move_xy(self, 0, 1),
            north_west: geohash_move_xy(self, -1, 1),
            north_east: geohash_move_xy(self, 1, 1),
            south_east: geohash_move_xy(self, 1, -1),
            south_west: geohash_move_xy(self, -1, -1),
        }
    }

    pub fn decode(self) -> Result<Point, GeoHashDecodeError> {
        geohash_decode_to_long_lat_wgs84(self).map(|coords| Point::try_from(coords).unwrap())
    }
}

impl fmt::Display for GeoHashBits {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let geoalphabet = "0123456789bcdefghjkmnpqrstuvwxyz".as_bytes();

        /* The internal format we use for geocoding is a bit different
         * than the standard, since we use as initial latitude range
         * -85,85, while the normal geohashing algorithm uses -90,90.
         * So we have to decode our position and re-encode using the
         * standard ranges in order to output a valid geohash string. */

        /* Decode... */
        let xy = geohash_decode_to_long_lat_wgs84(*self).map_err(|_| fmt::Error)?;

        /* Re-encode */
        let lon_range = GeoHashRange::from_min_and_max(-180.0, 180.0).unwrap();
        let lat_range = GeoHashRange::from_min_and_max(-90.0, 90.0).unwrap();

        let hash = geohash_encode(lon_range, lat_range, xy[0], xy[1], GEO_STEP_MAX).unwrap();

        let mut buf:Vec<u8> = vec![0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

        for i in 0..11 {
            let idx:usize;

            if i == 10 {
                /* We have just 52 bits, but the API used to output
                 * an 11 bytes geohash. For compatibility we assume
                 * zero. */
                idx = 0;
            } else {
                idx = ((hash.bits >> (52-((i+1)*5))) & 0x1f) as usize;
            }

            buf[i] = geoalphabet[idx];
        }

        write!(f, "{}", String::from_utf8(buf).unwrap())
    }
}

#[derive(Default,Copy,Clone)]
struct GeoHashRange {
    min: f64,
    max: f64,
}

impl GeoHashRange {
    fn new_long_range() -> GeoHashRange {
        GeoHashRange {
            min: GEO_LONG_MIN,
            max: GEO_LONG_MAX,
        }
    }

    fn new_lat_range() -> GeoHashRange {
        GeoHashRange {
            min: GEO_LAT_MIN,
            max: GEO_LAT_MAX,
        }
    }

    fn from_min_and_max(min: f64, max: f64) -> Result<GeoHashRange, ()> {
        if min >= max {
            return Err(())
        }

        Ok(GeoHashRange {
            min: min,
            max: max,
        })
    }

    fn is_zero(&self) -> bool {
        self.max == 0.0 && self.min == 0.0
    }
}

#[derive(Default)]
pub struct GeoHashArea {
    hash: GeoHashBits,
    longitude: GeoHashRange,
    latitude: GeoHashRange,
}

impl GeoHashArea {
    pub fn new() -> GeoHashArea {
        Default::default()
    }

    fn from_hash(hash: GeoHashBits) -> GeoHashArea {
        GeoHashArea {
            hash: hash,
            ..Default::default()
        }
    }
}

pub struct GeoHashNeighbors {
    pub north: GeoHashBits,
    pub east: GeoHashBits,
    pub west: GeoHashBits,
    pub south: GeoHashBits,
    pub north_east: GeoHashBits,
    pub south_east: GeoHashBits,
    pub north_west: GeoHashBits,
    pub south_west: GeoHashBits,
}

// Hashing works like this:
// Divide the world into 4 buckets.  Label each one as such:
//  -----------------
//  |       |       |
//  |       |       |
//  | 0,1   | 1,1   |
//  -----------------
//  |       |       |
//  |       |       |
//  | 0,0   | 1,0   |
//  -----------------

/// Interleave lower bits of x and y, so the bits of x
/// are in the even positions and bits from y in the odd;
/// x and y must initially be less than 2**32 (65536).
/// From:  https://graphics.stanford.edu/~seander/bithacks.html#InterleaveBMN
#[inline]
fn interleave64(xlo: u32, ylo: u32) -> u64 {
    const B: [u64;5] = [0x5555555555555555u64, 0x3333333333333333u64, 0x0F0F0F0F0F0F0F0Fu64, 0x00FF00FF00FF00FFu64, 0x0000FFFF0000FFFFu64];
    const S: [u32;5] = [1, 2, 4, 8, 16];

    let mut x: u64 = xlo as u64;
    let mut y: u64 = ylo as u64;

    x = (x | (x << S[4])) & B[4];
    y = (y | (y << S[4])) & B[4];

    x = (x | (x << S[3])) & B[3];
    y = (y | (y << S[3])) & B[3];

    x = (x | (x << S[2])) & B[2];
    y = (y | (y << S[2])) & B[2];

    x = (x | (x << S[1])) & B[1];
    y = (y | (y << S[1])) & B[1];

    x = (x | (x << S[0])) & B[0];
    y = (y | (y << S[0])) & B[0];

    x | (y << 1)
}

/// reverse the interleave process
/// derived from http://stackoverflow.com/questions/4909263
#[inline]
fn deinterleave64(interleaved: u64) -> u64 {
    const B: [u64;6] = [0x5555555555555555u64, 0x3333333333333333u64, 0x0F0F0F0F0F0F0F0Fu64, 0x00FF00FF00FF00FFu64, 0x0000FFFF0000FFFFu64, 0x00000000FFFFFFFFu64];
    const S: [u32;6] = [0, 1, 2, 4, 8, 16];

    let mut x: u64 = interleaved;
    let mut y: u64 = interleaved >> 1;

    x = (x | (x >> S[0])) & B[0];
    y = (y | (y >> S[0])) & B[0];

    x = (x | (x >> S[1])) & B[1];
    y = (y | (y >> S[1])) & B[1];

    x = (x | (x >> S[2])) & B[2];
    y = (y | (y >> S[2])) & B[2];

    x = (x | (x >> S[3])) & B[3];
    y = (y | (y >> S[3])) & B[3];

    x = (x | (x >> S[4])) & B[4];
    y = (y | (y >> S[4])) & B[4];

    x = (x | (x >> S[5])) & B[5];
    y = (y | (y >> S[5])) & B[5];

    x | (y << 32)
}

fn geohash_encode(long_range: GeoHashRange, lat_range: GeoHashRange, longitude: f64, latitude: f64, step: u8) -> Result<GeoHashBits, GeoHashEncodeError> {
    // Check basic arguments sanity.
    if step > 32 || step == 0 || lat_range.is_zero() || long_range.is_zero() {
        return Err(GeoHashEncodeError::ArgumentError);
    }

    // Return an error when trying to index outside the supported
    // constraints.
    // TODO remove this and accept a point as parameter
    if longitude > GEO_LONG_MAX || longitude < GEO_LONG_MIN || latitude > GEO_LAT_MAX || latitude < GEO_LAT_MIN {
        return Err(GeoHashEncodeError::CoordsOutOfBounds);
    }

    if latitude < lat_range.min || latitude > lat_range.max || longitude < long_range.min || longitude > long_range.max {
        return Err(GeoHashEncodeError::CoordsOutOfRange);
    }

    let mut lat_offset: f64 =
        (latitude - lat_range.min) / (lat_range.max - lat_range.min);
    let mut long_offset: f64 =
        (longitude - long_range.min) / (long_range.max - long_range.min);

    // convert to fixed point based on the step size
    lat_offset *= (1u64 << step) as f64;
    long_offset *= (1u64 << step) as f64;

    let bits = interleave64(lat_offset as u32, long_offset as u32);

    Ok(GeoHashBits::from_step_and_bits(step, bits))
}

fn geohash_encode_type(longitude: f64, latitude: f64, step: u8) -> Result<GeoHashBits, GeoHashEncodeError> {
    let r = [GeoHashRange::new_long_range(), GeoHashRange::new_lat_range()];

    geohash_encode(r[0], r[1], longitude, latitude, step)
}

pub fn geohash_encode_wgs84(longitude: f64, latitude: f64, step: u8) -> Result<GeoHashBits, GeoHashEncodeError> {
    geohash_encode_type(longitude, latitude, step)
}

fn geohash_decode(long_range: GeoHashRange, lat_range: GeoHashRange, hash: GeoHashBits) -> Result<GeoHashArea, GeoHashDecodeError> {
    if hash.is_zero() || lat_range.is_zero() || long_range.is_zero() {
        return Err(GeoHashDecodeError::ArgumentError);
    }

    let mut area = GeoHashArea::from_hash(hash);

    let step: u8 = hash.step;
    let hash_sep: u64 = deinterleave64(hash.bits); // hash = [LAT][LONG]

    let lat_scale: f64 = lat_range.max - lat_range.min;
    let long_scale: f64 = long_range.max - long_range.min;

    let ilato: u32 = hash_sep as u32;         // get lat part of deinterleaved hash
    let ilono: u32 = (hash_sep >> 32) as u32; // shift over to get long part of hash

    // divide by 2**step.
    // Then, for 0-1 coordinate, multiply times scale and add
    // to the min to get the absolute coordinate.
    area.latitude.min = lat_range.min + ((ilato as f64) / ((1u64 << step) as f64)) * lat_scale;
    area.latitude.max = lat_range.min + (((ilato + 1) as f64) / ((1u64 << step) as f64)) * lat_scale;
    area.longitude.min = long_range.min + ((ilono as f64) / ((1u64 << step) as f64)) * long_scale;
    area.longitude.max = long_range.min + (((ilono + 1) as f64) / ((1u64 << step) as f64)) * long_scale;

    Ok(area)
}

fn geohash_decode_type(hash: GeoHashBits) -> Result<GeoHashArea, GeoHashDecodeError> {
    geohash_decode(
        GeoHashRange::new_long_range(),
        GeoHashRange::new_lat_range(),
        hash,
    )
}

fn geohash_decode_area_to_long_lat(area: &GeoHashArea) -> [f64;2] {
    [
        (area.longitude.min + area.longitude.max) / 2.0,
        (area.latitude.min + area.latitude.max) / 2.0
    ]
}

fn geohash_decode_to_long_lat_type(hash: GeoHashBits) -> Result<[f64;2], GeoHashDecodeError> {
    let area = geohash_decode_type(hash)?;

    Ok(geohash_decode_area_to_long_lat(&area))
}

pub fn geohash_decode_to_long_lat_wgs84(hash: GeoHashBits) -> Result<[f64;2], GeoHashDecodeError> {
    geohash_decode_to_long_lat_type(hash)
}

fn geohash_move_x(hash: GeoHashBits, d: i8) -> GeoHashBits {
    if d == 0 {
        return hash;
    }

    let mut x: u64 = hash.bits & 0xaaaaaaaaaaaaaaaau64;
    let y: u64 = hash.bits & 0x5555555555555555u64;

    let zz: u64 = 0x5555555555555555u64 >> (64 - hash.step * 2);

    if d > 0 {
        x = x + (zz + 1);
    } else {
        x = x | zz;
        x = x - (zz + 1);
    }

    x &= 0xaaaaaaaaaaaaaaaau64 >> (64 - hash.step * 2);

    let mut new_hash = hash;

    new_hash.bits = x | y;

    new_hash
}

fn geohash_move_y(hash: GeoHashBits, d: i8) -> GeoHashBits {
    if d == 0 {
        return hash;
    }

    let x: u64 = hash.bits & 0xaaaaaaaaaaaaaaaau64;
    let mut y: u64 = hash.bits & 0x5555555555555555u64;

    let zz: u64 = 0xaaaaaaaaaaaaaaaau64 >> (64 - hash.step * 2);

    if d > 0 {
        y = y + (zz + 1);
    } else {
        y = y | zz;
        y = y - (zz + 1);
    }

    y &= 0x5555555555555555u64 >> (64 - hash.step * 2);

    let mut new_hash = hash;

    new_hash.bits = x | y;

    new_hash
}

fn geohash_move_xy(hash: GeoHashBits, dx: i8, dy: i8) -> GeoHashBits {
    geohash_move_x(geohash_move_y(hash, dy), dx)
}

#[cfg(test)]
mod tests {
    use crate::GEO_STEP_MAX;
    use super::{
        interleave64, deinterleave64, geohash_decode_to_long_lat_wgs84,
        geohash_encode_wgs84, GeoHashRange,
    };

    #[test]
    fn range_from_min_and_max() {
        let range = GeoHashRange::from_min_and_max(30.0, 20.0);

        assert!(range.is_err());
    }

    #[test]
    fn interleave() {
        assert_eq!(interleave64(1, 1), 3u64);
    }

    #[test]
    fn deinterleave() {
        let res = 4294967297u64;

        assert_eq!(deinterleave64(3u64), res);
        assert_eq!(res as u32, 1);
        assert_eq!((res >> 32) as u32, 1);
    }

    #[test]
    fn can_encode_decode() {
        let qlon = -96.92005634307861;
        let qlat = 19.547981570871666;
        let hash = geohash_encode_wgs84(qlon, qlat, GEO_STEP_MAX).unwrap();
        let coords = geohash_decode_to_long_lat_wgs84(hash).unwrap();

        assert!((qlon - coords[0]).abs() < 0.00001);
        assert!((qlat - coords[1]).abs() < 0.00001);
    }

    #[test]
    fn test_encode_decode_various_ponits() {
        let points = [
            [ -101.25, 52.26815737376817 ],
            [ -49.92187499999999, -12.554563528593656 ],
            [ 76.2890625, 70.02058730174062 ],
            [ 24.2578125, 7.710991655433217 ],
            [ 97.03125, -19.642587534013032 ],
            [ 110.0390625, 46.31658418182218 ],
            [ -45.703125, 69.65708627301174 ],
            [ 146.6015625, -48.224672649565186 ],
        ];

        for [lon, lat] in points.iter() {
            let hash = geohash_encode_wgs84(*lon, *lat, GEO_STEP_MAX).unwrap();
            let coords = geohash_decode_to_long_lat_wgs84(hash).unwrap();

            assert!((*lon - coords[0]).abs() < 0.00001);
            assert!((*lat - coords[1]).abs() < 0.00001);
        }
    }
}
