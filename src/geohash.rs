use crate::{GeoHashEncodeError, GeoHashBits, Point};
use std::collections::HashMap;

#[derive(Debug,PartialEq)]
pub struct QueryResult {
    name: String,
    dist: f64,
    point: Point,
}

pub struct GeoHash {
    dict: HashMap<String, GeoHashBits>,
}

impl GeoHash {
    pub fn new() -> GeoHash {
        GeoHash {
            dict: HashMap::new(),
        }
    }

    pub fn add(&mut self, name: &str, pos: Point) -> Result<(), GeoHashEncodeError> {
        self.dict.insert(name.to_string(), pos.encode()?);

        Ok(())
    }

    pub fn dist(&self, name_a: &str, name_b: &str) -> Option<f64> {
        self.dict.get(name_a).map(|hash_a| {
            self.dict.get(name_b).map(|hash_b| {
                hash_a.decode().unwrap().dist(hash_b.decode().unwrap())
            })
        }).flatten()
    }

    pub fn pos(&self, name: &str) -> Option<Point> {
        self.dict.get(name).map(|h| h.decode().unwrap())
    }

    pub fn hash(&self, name: &str) -> Option<String> {
        self.dict.get(name).map(|hash| format!("{}", hash))
    }

    pub fn radius(&self, pos: Point, radius: f64) -> Vec<QueryResult> {
        self.dict.iter().map(|(name, hash)| {
            let point = hash.decode().unwrap();

            QueryResult {
                name: name.to_string(),
                dist: pos.dist(point),
                point: point,
            }
        }).filter(|answer| {
            answer.dist < radius
        }).collect()
    }

    pub fn radius_by_member(&self, name: &str, radius: f64) -> Vec<QueryResult> {
        match self.dict.get(name) {
            Some(hash) => {
                let pos = hash.decode().unwrap();

                self.dict.iter().filter(|(iname, _hash)| *iname != name).map(|(name, hash)| {
                    let point = hash.decode().unwrap();

                    QueryResult {
                        name: name.to_string(),
                        dist: pos.dist(point),
                        point: point,
                    }
                }).filter(|answer| {
                    answer.dist < radius
                }).collect()
            }
            None => Vec::new(),
        }
    }
}

#[cfg(test)]
mod tests {
    use std::convert::TryFrom;
    use super::{GeoHash, Point, QueryResult};

    #[test]
    fn geoadd() {
        let mut geo = GeoHash::new();

        geo.add("a", Point::try_from([-20.0, 20.0]).unwrap()).unwrap();
    }

    #[test]
    fn geodist() {
        let mut geo = GeoHash::new();

        geo.add("a", Point::try_from([-75.343, 39.984]).unwrap()).unwrap();
        geo.add("b", Point::try_from([-75.534, 39.123]).unwrap()).unwrap();
        geo.add("c", Point::try_from([-96.87837219238281, 19.503870010375977]).unwrap()).unwrap();
        geo.add("d",Point::try_from([-96.87885284423828, 19.504638671875]).unwrap()).unwrap();

        assert!((geo.dist("a", "b").unwrap() - 97156.6783).abs() < 0.0001);
        assert!((geo.dist("c", "d").unwrap() - 99.2950).abs() < 0.1);
    }

    #[test]
    fn geopos() {
        let mut geo = GeoHash::new();

        geo.add("a", Point::try_from([-20.0, 20.0]).unwrap()).unwrap();

        assert_eq!(geo.pos("a").unwrap(), Point::try_from([-20.00000149011612, 20.000000589104864]).unwrap());
    }

    #[test]
    fn geohash() {
        let mut geo = GeoHash::new();

        geo.add("a", Point::try_from([-20.0, 20.0]).unwrap()).unwrap();

        assert_eq!(geo.hash("a").unwrap(), "ee9cbbukqn0".to_string());
    }

    #[test]
    fn georadius() {
        let mut geo = GeoHash::new();

        geo.add("a", Point::try_from([-96.87837219238281, 19.503870010375977]).unwrap()).unwrap();
        geo.add("b", Point::try_from([-96.87885284423828, 19.504638671875]).unwrap()).unwrap();
        geo.add("c", Point::try_from([-96.87889099121094, 19.506189346313477]).unwrap()).unwrap();
        geo.add("e", Point::try_from([-96.87905883789062, 19.506153106689453]).unwrap()).unwrap();
        geo.add("f", Point::try_from([-96.87897491455078, 19.506465911865234]).unwrap()).unwrap();
        geo.add("g", Point::try_from([-96.87831115722656, 19.506223678588867]).unwrap()).unwrap();
        geo.add("h", Point::try_from([-96.87734985351562, 19.50629234313965]).unwrap()).unwrap();
        geo.add("i", Point::try_from([-96.87670135498047, 19.506750106811523]).unwrap()).unwrap();
        geo.add("j", Point::try_from([-96.87612915039062, 19.507585525512695]).unwrap()).unwrap();

        assert_eq!(
            geo.radius(Point::try_from([-96.87904357910156, 19.506000518798828]).unwrap(), 20.0),
            vec![QueryResult {
                name: "e".to_string(),
                point: Point::try_from([-96.87905877828598, 19.506153261467382]).unwrap(),
                dist: 17.063572207713385,
            }],
        );
    }

    #[test]
    fn georadiusbymember() {
        let mut geo = GeoHash::new();

        geo.add("a", Point::try_from([-96.87837219238281, 19.503870010375977]).unwrap()).unwrap();
        geo.add("b", Point::try_from([-96.87885284423828, 19.504638671875]).unwrap()).unwrap();
        geo.add("c", Point::try_from([-96.87889099121094, 19.506189346313477]).unwrap()).unwrap();
        geo.add("d", Point::try_from([-96.87904357910156, 19.506000518798828]).unwrap()).unwrap();
        geo.add("e", Point::try_from([-96.87905883789062, 19.506153106689453]).unwrap()).unwrap();
        geo.add("f", Point::try_from([-96.87897491455078, 19.506465911865234]).unwrap()).unwrap();
        geo.add("g", Point::try_from([-96.87831115722656, 19.506223678588867]).unwrap()).unwrap();
        geo.add("h", Point::try_from([-96.87734985351562, 19.50629234313965]).unwrap()).unwrap();
        geo.add("i", Point::try_from([-96.87670135498047, 19.506750106811523]).unwrap()).unwrap();
        geo.add("j", Point::try_from([-96.87612915039062, 19.507585525512695]).unwrap()).unwrap();

        assert_eq!(
            geo.radius_by_member("d", 20.0),
            vec![QueryResult {
                name: "e".to_string(),
                point: Point::try_from([-96.87905877828598, 19.506153261467382]).unwrap(),
                dist: 16.999599189291942,
            }],
        );
    }
}
