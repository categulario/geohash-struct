#[derive(Debug)]
pub enum GeoHashEncodeError {
    ArgumentError,
    CoordsOutOfBounds,
    CoordsOutOfRange,
}

#[derive(Debug)]
pub enum GeoHashDecodeError {
    ArgumentError,
}
