use std::convert::TryFrom;

use crate::{
    GEO_LONG_MAX, GEO_LONG_MIN, GEO_LAT_MAX, GEO_LAT_MIN, GEO_STEP_MAX,
    EARTH_RADIUS_IN_METERS, GeoHashBits, GeoHashEncodeError,
    geohash_encode_wgs84,
};

#[derive(Debug,PartialEq)]
pub enum CoordinateError {
    ExceededBounds,
}

#[derive(Debug,Copy,Clone,PartialEq)]
pub struct Point {
    lon: f64,
    lat: f64,
}

impl Point {
    pub fn lon(&self) -> f64 {
        self.lon
    }

    pub fn lat(&self) -> f64 {
        self.lat
    }

    pub fn coords(&self) -> [f64;2] {
        [self.lon, self.lat]
    }

    pub fn encode(&self) -> Result<GeoHashBits, GeoHashEncodeError> {
        geohash_encode_wgs84(self.lon, self.lat, GEO_STEP_MAX)
    }

    /// Calculate distance using haversin great circle distance formula.
    /// returned value in meters.
    pub fn dist(&self, other: Point) -> f64 {
        let [lon1d, lat1d] = self.coords();
        let [lon2d, lat2d] = other.coords();

        let lat1r = lat1d.to_radians();
        let lon1r = lon1d.to_radians();
        let lat2r = lat2d.to_radians();
        let lon2r = lon2d.to_radians();
        let u = ((lat2r - lat1r) / 2.0).sin();
        let v = ((lon2r - lon1r) / 2.0).sin();

        return 2.0 * EARTH_RADIUS_IN_METERS * ((u * u + (lat1r).cos() * (lat2r).cos() * v * v).sqrt()).asin();
    }
}

impl TryFrom<[f64;2]> for Point {
    type Error = CoordinateError;

    fn try_from(value: [f64;2]) -> Result<Self, Self::Error> {
        if value[0] > GEO_LONG_MAX || value[0] < GEO_LONG_MIN || value[1] > GEO_LAT_MAX || value[1] < GEO_LAT_MIN {
            return Err(CoordinateError::ExceededBounds);
        }

        Ok(Point {
            lon: value[0],
            lat: value[1],
        })
    }
}

#[cfg(test)]
mod tests {
    use std::convert::TryFrom;
    use super::{Point, CoordinateError};

    #[test]
    fn test_point_from_array() {
        let p = Point::try_from([-10.0, 20.0]).unwrap();

        assert_eq!(p.lon(), -10.0);
        assert_eq!(p.lat(), 20.0);
    }

    #[test]
    fn test_exceeded_bound() {
        let e = Point::try_from([-200.0, 30.0]).unwrap_err();

        assert_eq!(e, CoordinateError::ExceededBounds);
    }

    #[test]
    fn test_distance_between_points() {
        let p1 = Point::try_from([-96.87837219238281, 19.503870010375977]).unwrap();
        let p2 = Point::try_from([-96.87885284423828, 19.504638671875]).unwrap();

        // error less than 10 cm compared to redis answer to the same question
        assert!((p1.dist(p2) - 99.2950).abs() < 0.1);
    }
}
