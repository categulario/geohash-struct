mod hash;
mod geohash;
mod error;
mod types;

// Limits from EPSG:900913 / EPSG:3785 / OSGEO:41001
pub const GEO_LAT_MIN: f64 = -85.05112878;
pub const GEO_LAT_MAX: f64 = 85.05112878;
pub const GEO_LONG_MIN: f64 = -180.0;
pub const GEO_LONG_MAX: f64 = 180.0;

pub const GEO_STEP_MAX: u8 = 26; // 26*2 = 52 bits.

pub const EARTH_RADIUS_IN_METERS: f64 = 6372797.560856;

pub use types::Point;

pub use error::{GeoHashEncodeError, GeoHashDecodeError};

pub use hash::{
    GeoHashBits, geohash_decode_to_long_lat_wgs84, geohash_encode_wgs84,
    GeoDirection, GeoHashArea,
};

pub use geohash::GeoHash;
